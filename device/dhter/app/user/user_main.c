
// 头文件引用
//==================================================================================
#include "user_config.h"		// 用户配置
#include "driver/uart.h"  		// 串口
#include "driver/oled.h"  		// OLED
#include "driver/dht11.h"		// DHT11头文件

//#include "at_custom.h"
#include "c_types.h"			// 变量类型
#include "eagle_soc.h"			// GPIO函数、宏定义
#include "ip_addr.h"			// 被"espconn.h"使用。在"espconn.h"开头#include"ip_addr.h"或#include"ip_addr.h"放在"espconn.h"之前
#include "espconn.h"			// TCP/UDP接口
//#include "espnow.h"
#include "ets_sys.h"			// 回调函数
//#include "gpio.h"
#include "mem.h"				// 内存申请等函数
#include "os_type.h"			// os_XXX
#include "osapi.h"  			// os_XXX、软件定时器
//#include "ping.h"
//#include "pwm.h"
//#include "queue.h"
#include "smartconfig.h"
#include "sntp.h"
//#include "spi_flash.h"
//#include "upgrade.h"
#include "user_interface.h" 	// 系统接口、system_param_xxx接口、WIFI、RateContro
//==================================================================================


// 宏定义
//==================================================================================
#define		ProjectName			"dhter"		// 工程名宏定义

#define		ESP8266_STA_SSID	"XXXX"			// 接入的WIFI名
#define		ESP8266_STA_PASS	"XXXX"		// 接入的WIFI密码

#define		DHTER_DEFAULT_REPORT_INTERVAL 300000						//default set to 5 min

#define		LED_ON				GPIO_OUTPUT_SET(GPIO_ID_PIN(4),0)		// LED亮
#define		LED_OFF				GPIO_OUTPUT_SET(GPIO_ID_PIN(4),1)		// LED灭


#define		Sector_STA_INFO		0x90			// 【STA参数】保存扇区

//==================================================================================
typedef struct sntpData_t
{
	uint8 week;
	uint8 month;
	uint8 day;
	uint8 hour;
	uint8 minute;
	uint8 second;
	uint8 year;
} SntpData_t;

// 全局变量
//==================================================================================
os_timer_t dht_timer;			// 定义软件定时器

struct espconn ST_NetCon;		// 网络连接结构体

u8 C_LED_Flash = 0;				// LED闪烁计次

//函数声明
//==================================================================================
void ICACHE_FLASH_ATTR timer_init(u32 time_ms, u8 time_repetitive, void *OS_Timer_cb);
void ICACHE_FLASH_ATTR socket_tcp_init(void);

// 毫秒延时函数
//===========================================
void ICACHE_FLASH_ATTR delay_ms(u32 C_time)
{	for(;C_time>0;C_time--)
		os_delay_us(1000);
}

/**
 @brief 检查星期几
 @param pWeek -[in] 待检测字符串
 @return 星期几
*/
static uint8 ICACHE_FLASH_ATTR checkWahtWeek(char *pWeek)
{
	if(!pWeek)
	{
		return 0;
	}

	uint8 week;
	if(strcmp(pWeek, "Mon") == 0)
	{
		week = 1;									// 星期一
	}
	else if(strcmp(pWeek, "Tue") == 0)
	{
		week = 2;									// 星期二
	}
	else if(strcmp(pWeek, "Wed") == 0)
	{
		week = 3;									// 星期三
	}
	else if(strcmp(pWeek, "Thu") == 0)
	{
		week = 4;									// 星期四
	}
	else if(strcmp(pWeek, "Fri") == 0)
	{
		week = 5;									// 星期五
	}
	else if(strcmp(pWeek, "Sat") == 0)
	{
		week = 6;									// 星期六
	}
	else if(strcmp(pWeek, "Sun") == 0)
	{
		week = 7;									// 星期天
	}
	return week;
}

/**
 @brief 检查几月份
 @param pMonth -[in] 待检测字符串
 @return 几月份
*/
static uint8 ICACHE_FLASH_ATTR checkWahtMonth(char *pMonth)
{
	if(!pMonth)
	{
		return 0;
	}

	uint8 month;
	if(strcmp(pMonth, "Jan") == 0)
	{
		month = 1;									// 一月
	}
	else if(strcmp(pMonth, "Feb") == 0)
	{
		month = 2;									// 二月
	}
	else if(strcmp(pMonth, "Mar") == 0)
	{
		month = 3;									// 三月
	}
	else if(strcmp(pMonth, "Apr") == 0)
	{
		month = 4;									// 四月
	}
	else if(strcmp(pMonth, "May") == 0)
	{
		month = 5;									// 五月
	}
	else if(strcmp(pMonth, "Jun") == 0)
	{
		month = 6;									// 六月
	}
	else if(strcmp(pMonth, "Jul") == 0)
	{
		month = 7;									// 七月
	}
	else if(strcmp(pMonth, "Aug") == 0)
	{
		month = 8;									// 八月
	}
	else if(strcmp(pMonth, "Sep") == 0)
	{
		month = 9;									// 九月
	}
	else if(strcmp(pMonth, "Oct") == 0)
	{
		month = 10;									// 十月
	}
	else if(strcmp(pMonth, "Nov") == 0)
	{
		month = 11;									// 十一月
	}
	else if(strcmp(pMonth, "Dec") == 0)
	{
		month = 12;									// 十二月
	}
	return month;
}

/**
 @brief 检查几日
 @param pDay -[in] 待检测字符串
 @return 几日
*/
static uint8 ICACHE_FLASH_ATTR checkWahtDay(char *pDay)
{
	if(!pDay)
	{
		return 0;
	}

	uint8 day = (*pDay & 0x0f) << 4;
	day = day | (*(pDay+1) & 0x0f);

	return day;
}

/**
 @brief 检查几时
 @param pHour -[in] 待检测字符串
 @return 几时
*/
static uint8 ICACHE_FLASH_ATTR checkWahtHour(char *pHour)
{
	if(!pHour)
	{
		return 0;
	}

	uint8 hour = (*pHour & 0x0f) << 4;
	hour = hour | (*(pHour+1) & 0x0f);

	return hour;
}

/**
 @brief 检查几分
 @param pMinute -[in] 待检测字符串
 @return 几分
*/
static uint8 ICACHE_FLASH_ATTR checkWahtMinute(char *pMinute)
{
	if(!pMinute)
	{
		return 0;
	}

	uint8 minute = (*pMinute & 0x0f) << 4;
	minute = minute | (*(pMinute+1) & 0x0f);

	return minute;
}

/**
 @brief 检查几秒
 @param pSecond -[in] 待检测字符串
 @return 几秒
*/
static uint8 ICACHE_FLASH_ATTR checkWahtSecond(char *pSecond)
{
	if(!pSecond)
	{
		return 0;
	}

	uint8 second = (*pSecond & 0x0f) << 4;
	second = second | (*(pSecond+1) & 0x0f);

	return second;
}

/**
 @brief 检查几年
 @param pYear -[in] 待检测字符串
 @return 几年
*/
static uint8 ICACHE_FLASH_ATTR checkWahtYear(char *pYear)
{
	if(!pYear)
	{
		return 0;
	}

	uint8 year = (*(pYear+2) & 0x0f) << 4;
	year = year | (*(pYear+3) & 0x0f);

	return year;
}
/**
 @brief SNTP时间转化为简单日期格式
 @param pSntpRealTime -[in] 实时时间
 @return 简单日期格式时间字符串
*/
static u8 ICACHE_FLASH_ATTR sntp_to_simple_date_format(char *pSntpRealTime, char *pFormatTimeStamp)
{
	u8 len = 0;
	if(!pSntpRealTime)
	{
		return len;
	}

	pSntpRealTime[24] = '\0';						// 不要年份后面的数据
	SntpData_t sntpData;
	uint8 dateType[7] = {3, 3, 2, 2, 2, 2, 4};		// 3-Fri 3-May 2-31 2-11: 2-21: 2-42 4-2019
	uint8 temp[5];
	uint8 i = 0, j = 0;

	while(*pSntpRealTime != '\0')
	{
		if(*pSntpRealTime == ' ' || *pSntpRealTime == ':')
		{
			pSntpRealTime++;
			i++;
		}
		else
		{
			os_memset(temp, '\0', 5);
			for(j = 0; j < dateType[i]; j++)
			{
				temp[j] = *pSntpRealTime;
				pSntpRealTime++;
			}
			switch(i)
			{
			case 0: // 周
				sntpData.week = checkWahtWeek(temp);
				break;
			case 1: // 月
				sntpData.month = checkWahtMonth(temp);
				break;
			case 2: // 日
				sntpData.day = checkWahtDay(temp);
				break;
			case 3: // 时
				sntpData.hour = checkWahtHour(temp);
				break;
			case 4: // 分
				sntpData.minute = checkWahtMinute(temp);
				break;
			case 5: // 秒
				sntpData.second = checkWahtSecond(temp);
				break;
			case 6: // 年
				sntpData.year = checkWahtYear(temp);
				break;
			default:
				break;
			}
		}
	}

	os_memset(pFormatTimeStamp, 0, 32);
	len = os_sprintf(pFormatTimeStamp, "20%x-%02x-%02x %02x:%02x:%02x",
								sntpData.year, sntpData.month,
								sntpData.day, sntpData.hour,
								sntpData.minute, sntpData.second);
	return len;
}

/*
 * 获取SNTP时间并转化为"Y-m-d h:m:s" 格式
 *
 * */
int ICACHE_FLASH_ATTR get_current_time(char *timeStamp)
{
	u8 ret = 0;
	char formatTimeStamp[32] = {0};
	uint32	TimeStamp;
	char * realTime;


	 //Get the timestamp(second) from 1970.01.01 00:00:00 GMT+8
	 //-----------------------------------------------------------------
	 TimeStamp = sntp_get_current_timestamp();

	 if(TimeStamp)
	 {
		 realTime = sntp_get_real_time(TimeStamp);
		 ret = sntp_to_simple_date_format(realTime, formatTimeStamp);

		 // timeStamp is not NULL point and sntp_to_simple_date_format transform the timestamp success

		 if(timeStamp != NULL && ret != 0)
		 {
			 os_memset(timeStamp,0x00,sizeof(timeStamp));
			 os_memcpy(timeStamp, formatTimeStamp, os_strlen(formatTimeStamp));
		 }
	 }
	 return ret;
}


// ESP8266_STA connect to wifi with default wifi ssid and pwd
//==============================================================================
void ICACHE_FLASH_ATTR wifi_init(void *cb)
{
	struct station_config STA_Config;	// STA参数结构体

	struct ip_info ST_ESP8266_IP;		// STA信息结构体

	// 设置ESP8266的工作模式
	wifi_set_opmode(0x01);				// 设置为STA模式，并保存到Flash

	os_memset(&STA_Config, 0, sizeof(struct station_config));	// STA参数结构体 = 0
	os_strcpy(STA_Config.ssid,ESP8266_STA_SSID);				// 设置WIFI名
	os_strcpy(STA_Config.password,ESP8266_STA_PASS);			// 设置WIFI密码

	wifi_station_set_config(&STA_Config);	// Save wifi ssid and password to Flash

	timer_init(1000, 1, cb);		//Start timer to check if wifi is connected
}
//=========================================================================================

// 成功发送网络数据的回调函数
//==========================================================
void ICACHE_FLASH_ATTR socket_tcp_sent_cb(void *arg)
{
	os_printf("ESP8266_WIFI_Send_OK\n");
}

typedef enum{
	e_min = 0x00,
	e_hello = 0x01,
	e_ssid = 0x02,
	e_ssid_pwd = 0x03,
	e_remote_server = 0x04,
	e_report_interval=0x05,
	e_device_name=0x06,
	e_soft_ver=0x07,
	e_max
}e_cmd_type;

// 成功接收网络数据的回调函数【参数1：网络传输结构体espconn指针、参数2：网络传输数据指针、参数3：数据长度】
//=========================================================================================================
void ICACHE_FLASH_ATTR socket_tcp_recv_cb(void * arg, char * pdata, unsigned short len)
{
	struct espconn * T_arg = arg;		// 缓存网络连接结构体指针
	e_cmd_type remote_cmd = (e_cmd_type)pdata[0];
	switch(remote_cmd)
	{
		case e_hello:
			espconn_send(T_arg,"hello back",os_strlen("hello back"));
			LED_ON;
			delay_ms(1000);
			LED_OFF;
			break;
		case e_ssid:

			break;
		case e_ssid_pwd:

			break;
		case e_remote_server:

			break;

		case e_report_interval:

			break;
		case e_device_name:

			break;
		case e_soft_ver:

			break;
		default:
			os_printf("Invalid cmd:%x\n",remote_cmd);
			break;
	}
	//【TCP通信是面向连接的，向远端主机回应时可直接使用T_arg结构体指针指向的IP信息】
	//-----------------------------------------------------------------------------------------------
	espconn_send(T_arg,"ESP8266_WIFI_Recv_OK",os_strlen("ESP8266_WIFI_Recv_OK"));	// 向对方发送应答
}
//=========================================================================================================


// TCP连接断开成功的回调函数
//================================================================
void ICACHE_FLASH_ATTR socket_tcp_disconnect_cb(void *arg)
{
	os_printf("\n--------------- ESP8266_TCP_Disconnect_OK ---------------\n");
	//restart timer after the cb;
	timer_init(DHTER_DEFAULT_REPORT_INTERVAL, 1, socket_tcp_init);
}
//================================================================


// TCP连接建立成功的回调函数
//====================================================================================================================
void ICACHE_FLASH_ATTR socket_tcp_connected_cb(void *arg)
{
	char buff[128] = {0};
	char timeStamp[32] = {0};
	char temp_str[32] = {0};
	char humi_str[32] = {0};

	espconn_regist_sentcb((struct espconn *)arg, socket_tcp_sent_cb);			// 注册网络数据发送成功的回调函数
	espconn_regist_recvcb((struct espconn *)arg, socket_tcp_recv_cb);			// 注册网络数据接收成功的回调函数
	espconn_regist_disconcb((struct espconn *)arg,socket_tcp_disconnect_cb);	// 注册成功断开TCP连接的回调函数

	os_printf("\n--------------- ESP8266_TCP_Connect_OK ---------------\n");
	if(get_current_time(timeStamp))
	{
		;
	}

	if(DHT11_Read_Data_Complete() == 0)		// 读取DHT11温湿度值
	{
		// 温度超过30℃，LED亮
		//----------------------------------------------------
		if(DHT11_Data_Array[5]==1 && DHT11_Data_Array[2]>=30)
		{
			GPIO_OUTPUT_SET(GPIO_ID_PIN(4),0);		// LED亮
		}
		else
		{
			GPIO_OUTPUT_SET(GPIO_ID_PIN(4),1);		// LED灭
		}

		// 串口输出温湿度
		//---------------------------------------------------------------------------------
		memset(buff,0x00,strlen(buff));

		if(DHT11_Data_Array[5] == 1)			// 温度 >= 0℃
		{
			os_printf("温度 =%d.%d ℃, 湿度 =%d.%d %RH,timeStamp=%s\n",DHT11_Data_Array[2],DHT11_Data_Array[3],DHT11_Data_Array[0],DHT11_Data_Array[1],timeStamp);
			ets_sprintf(buff,"LJF0001,%d.%d,%d.%d,%s\n",DHT11_Data_Array[2],DHT11_Data_Array[3],DHT11_Data_Array[0],DHT11_Data_Array[1],timeStamp);
		}
		else // if(DHT11_Data_Array[5] == 0)	// 温度 < 0℃
		{
			os_printf("温度 =-%d.%d ℃, 湿度 =%d.%d %RH, timeStamp=%s\n",DHT11_Data_Array[2],DHT11_Data_Array[3],DHT11_Data_Array[0],DHT11_Data_Array[1],timeStamp);
			ets_sprintf(buff,"LJF0001,-%d.%d,%d.%d,%s\n",DHT11_Data_Array[2],DHT11_Data_Array[3],DHT11_Data_Array[0],DHT11_Data_Array[1],timeStamp);
		}

		// OLED show th info
		DHT11_NUM_Char();	// DHT11 data array to string

		os_printf(temp_str,"Temp:%s",DHT11_Data_Char[0]);
		os_printf(humi_str,"Humi:%s",DHT11_Data_Char[1]);
		OLED_ShowString(0,4,temp_str);
		OLED_ShowString(0,6,humi_str);
	}

	espconn_send((struct espconn *)arg, buff, os_strlen(buff));	// 打招呼

	//send data OK, then disconnect the tcp connection
	espconn_disconnect((struct espconn *)arg);
}

//====================================================================================================================

// 初始化SNTP
//=============================================================================
void ICACHE_FLASH_ATTR sntp_service_init(void *cb)
{
	ip_addr_t * addr = (ip_addr_t *)os_zalloc(sizeof(ip_addr_t));

	sntp_setservername(0, "us.pool.ntp.org");
	sntp_setservername(1, "ntp.sjtu.edu.cn");

	ipaddr_aton("210.72.145.44", addr);			// 点分十进制 => 32位二进制
	sntp_setserver(2, addr);					// 服务器_2【IP地址】
	os_free(addr);								// 释放addr

	sntp_init();	// SNTP初始化API
	timer_init(3000, 1, cb);
}

void ICACHE_FLASH_ATTR sntp_service_cb(void *arg)
{
	if(sntp_get_current_timestamp())		// 判断是否获取到偏移时间
	{
		OLED_ShowString(0,2,"Init SNTP OK...");
		socket_tcp_init();		// 初始化网络连接(TCP通信)
	}
	else
	{
		os_printf("Geting sntp timestamp...\n");
	}
}
//=============================================================================
// SmartConfig状态发生改变时，进入此回调函数
//--------------------------------------------
// 参数1：sc_status status / 参数2：无类型指针【在不同状态下，[void *pdata]的传入参数是不同的】
//=================================================================================================================
void ICACHE_FLASH_ATTR smartconfig_done(sc_status status, void *pdata)
{
	os_printf("\r\n------ smartconfig_done ------\r\n");	// ESP8266网络状态改变

    switch(status)
    {
    	// CmartConfig等待
		//……………………………………………………
		case SC_STATUS_WAIT:		// 初始值
			os_printf("\r\nSC_STATUS_WAIT\r\n");
		break;
		//……………………………………………………

		// 发现【WIFI信号】（8266在这种状态下等待配网）
		//…………………………………………………………………………………………………
		case SC_STATUS_FIND_CHANNEL:
			os_printf("\r\nSC_STATUS_FIND_CHANNEL\r\n");

			os_printf("\r\n---- Please Use WeChat to SmartConfig ------\r\n\r\n");

			OLED_ShowString(0,4,"Use WeChat to   ");
			OLED_ShowString(0,6,"SmartConfig     ");
		break;
		//…………………………………………………………………………………………………

        // 正在获取【SSID】【PSWD】
        //…………………………………………………………………………………………………
        case SC_STATUS_GETTING_SSID_PSWD:
            os_printf("\r\nSC_STATUS_GETTING_SSID_PSWD\r\n");

            // 【SC_STATUS_GETTING_SSID_PSWD】状态下，参数2==SmartConfig类型指针
            //-------------------------------------------------------------------
			sc_type *type = pdata;		// 获取【SmartConfig类型】指针

			// 配网方式 == 【ESPTOUCH】
			//-------------------------------------------------
            if (*type == SC_TYPE_ESPTOUCH)
            { os_printf("\r\nSC_TYPE:SC_TYPE_ESPTOUCH\r\n"); }

            // 配网方式 == 【AIRKISS】||【ESPTOUCH_AIRKISS】
            //-------------------------------------------------
            else
            { os_printf("\r\nSC_TYPE:SC_TYPE_AIRKISS\r\n"); }

	    break;
	    //…………………………………………………………………………………………………

	    // 成功获取到【SSID】【PSWD】，保存STA参数，并连接WIFI
	    //…………………………………………………………………………………………………
        case SC_STATUS_LINK:
            os_printf("\r\nSC_STATUS_LINK\r\n");

            // 【SC_STATUS_LINK】状态下，参数2 == STA参数指针
            //------------------------------------------------------------------
            struct station_config *sta_conf = pdata;	// 获取【STA参数】指针

            // 将【SSID】【PASS】保存到【外部Flash】中
            //--------------------------------------------------------------------------
			spi_flash_erase_sector(Sector_STA_INFO);						// 擦除扇区
			spi_flash_write(Sector_STA_INFO*4096, (uint32 *)sta_conf, 96);	// 写入扇区
			//--------------------------------------------------------------------------

	        wifi_station_set_config(sta_conf);			// 设置STA参数【Flash】
	        wifi_station_disconnect();					// 断开STA连接
	        wifi_station_connect();						// ESP8266连接WIFI

	    	OLED_ShowString(0,4,"WIFI Connecting ");	// OLED显示：
	    	OLED_ShowString(0,6,"................");	// 正在连接WIFI

	    break;
	    //…………………………………………………………………………………………………


        // ESP8266作为STA，成功连接到WIFI
	    //…………………………………………………………………………………………………
        case SC_STATUS_LINK_OVER:
            os_printf("\r\nSC_STATUS_LINK_OVER\r\n");

            smartconfig_stop();		// stop SmartConfig

			for(C_LED_Flash=0; C_LED_Flash<=20; C_LED_Flash++)
			{
				GPIO_OUTPUT_SET(GPIO_ID_PIN(4),(C_LED_Flash%2));
				delay_ms(100);
			}

			sntp_service_init(&sntp_service_cb);

			os_printf("\r\n---- ESP8266 Connect to WIFI Successfully ----\r\n");
			//**************************************************************************************************

	    break;
	    //…………………………………………………………………………………………………

    }
}


// TCP连接异常断开时的回调函数【ESP8266作为TCP_Client，连接TCP_Server失败，也会进入此函数】
//==========================================================================================
void ICACHE_FLASH_ATTR socket_tcp_break_cb(void *arg,sint8 err)
{
	os_printf("ESP8266_TCP_Break reconnect to server\n");

	espconn_connect(&ST_NetCon);	// 连接TCP-server
}
//==========================================================================================


// 定义espconn型结构体
//-----------------------------------------------
//struct espconn ST_NetCon;	// 网络连接结构体

// 初始化网络连接(TCP通信)
//=========================================================================================================
void ICACHE_FLASH_ATTR socket_tcp_init()
{
	// 结构体赋值
	//--------------------------------------------------------------------------
	ST_NetCon.type = ESPCONN_TCP ;				// 设置为TCP协议

	ST_NetCon.proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));	// 开辟内存


	// 此处需要设置目标IP/端口(ESP8266作为Client，需要预先知道Server的IP/端口)
	//-------------------------------------------------------------------------
	ST_NetCon.proto.tcp->local_port = 8266 ;	// 设置本地端口

	ST_NetCon.proto.tcp->remote_port = 1314;	// 设置目标端口
	ST_NetCon.proto.tcp->remote_ip[0] = 81;	// 设置目标IP地址
	ST_NetCon.proto.tcp->remote_ip[1] = 69;
	ST_NetCon.proto.tcp->remote_ip[2] = 57;
	ST_NetCon.proto.tcp->remote_ip[3] = 134;

	//u8 remote_ip[4] = {192,168,8,47};		// 目标ip地址
	//os_memcpy(ST_NetCon.proto.udp->remote_ip,remote_ip,4);	// 拷贝内存


	// 注册连接成功回调函数、异常断开回调函数
	//--------------------------------------------------------------------------------------------------
	espconn_regist_connectcb(&ST_NetCon, socket_tcp_connected_cb);	// 注册TCP连接成功建立的回调函数
	espconn_regist_reconcb(&ST_NetCon, socket_tcp_break_cb);		// 注册TCP连接异常断开的回调函数


	// 连接 TCP server
	//----------------------------------------------------------
	os_printf("Start connect to server\n");
	espconn_connect(&ST_NetCon);	// 连接TCP-server
}

// wifi_connecting_check_cb used to check if wifi conect to the default ssid if not switch to smart link state
//=========================================================================================================
void ICACHE_FLASH_ATTR wifi_connecting_check_cb(void)
{
	struct ip_info ST_ESP8266_IP;
	u8 led_flash_counter = 0;
	u8 ESP8266_IP[4];
	u8 S_WIFI_STA_Connect = wifi_station_get_connect_status();

	if( S_WIFI_STA_Connect == STATION_GOT_IP )
	{
		os_timer_disarm(&dht_timer);	// 关闭定时器
		wifi_get_ip_info(STATION_IF,&ST_ESP8266_IP);	// get STA IP info
		ESP8266_IP[0] = ST_ESP8266_IP.ip.addr;
		ESP8266_IP[1] = ST_ESP8266_IP.ip.addr>>8;
		ESP8266_IP[2] = ST_ESP8266_IP.ip.addr>>16;
		ESP8266_IP[3] = ST_ESP8266_IP.ip.addr>>24;

		// show ESP8266's IP address
		//-----------------------------------------------------------------------------------------------
		os_printf("ESP8266_IP = %d.%d.%d.%d\n",ESP8266_IP[0],ESP8266_IP[1],ESP8266_IP[2],ESP8266_IP[3]);
		OLED_ShowString(0,2,"Init Wifi OK....");
		//OLED_ShowIP(24,0,ESP8266_IP);	// show ip address via OLED

		// WIFI connected，LED blink 10 times
		for(led_flash_counter=0; led_flash_counter<=20; led_flash_counter++)
		{
			GPIO_OUTPUT_SET(GPIO_ID_PIN(4), (led_flash_counter%2));
			delay_ms(100);
		}

		sntp_service_init(&sntp_service_cb);		// 初始化SNTP

	}
	// device could not connect to wifi then switch to smart link mode
	//------------------------------------------------------------------------------------------------
	else if(	S_WIFI_STA_Connect==STATION_NO_AP_FOUND 	||		// 未找到指定WIFI
				S_WIFI_STA_Connect==STATION_WRONG_PASSWORD 	||		// WIFI密码错误
				S_WIFI_STA_Connect==STATION_CONNECT_FAIL		)	// 连接WIFI失败
	{
		os_timer_disarm(&dht_timer);			// stop timer

		os_printf("\r\n---- device Can't Connect to WIFI -----------\r\n");

		// 微信智能配网设置
		//…………………………………………………………………………………………………………………………
		wifi_set_opmode(STATION_MODE);		// 设为STA模式							//【第①步】

		smartconfig_set_type(SC_TYPE_AIRKISS); 	// ESP8266配网方式【AIRKISS】			//【第②步】

		smartconfig_start(smartconfig_done);	// 进入【智能配网模式】,并设置回调函数	//【第③步】
		//…………………………………………………………………………………………………………………………
	}
	else
	{
		os_printf("Invalid wifi connect state:%d.\n",S_WIFI_STA_Connect);
	}
}
//=========================================================================================================

// 软件定时器初始化(ms毫秒)
//=====================================================================================
void ICACHE_FLASH_ATTR timer_init(u32 time_ms, u8 time_repetitive, void *OS_Timer_cb)
{
	os_timer_disarm(&dht_timer);	// 关闭定时器
	os_timer_setfn(&dht_timer,(os_timer_func_t *)OS_Timer_cb, NULL);	// 设置定时器
	os_timer_arm(&dht_timer, time_ms, time_repetitive);  // 使能定时器
}
//=====================================================================================

// LED初始化
//=============================================================================
void ICACHE_FLASH_ATTR gpio_init(void)
{
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U,	FUNC_GPIO4);	// GPIO4设为IO口

	GPIO_OUTPUT_SET(GPIO_ID_PIN(4),1);						// IO4 = 1
}
//=============================================================================


// user_init：entry of user application, init user function here
//==============================================================================
void ICACHE_FLASH_ATTR user_init(void)
{
	uart_init(115200,115200);	// 初始化串口波特率
	os_delay_us(10000);			// 等待串口稳定
	os_printf("\r\n=================================================\r\n");
	os_printf("\t Project:\t%s\r\n", ProjectName);
	os_printf("\t SDK version:\t%s", system_get_sdk_version());
	os_printf("\r\n=================================================\r\n");

	// OLED显示初始化
	//--------------------------------------------------------
	OLED_Init();							    // OLED初始化
	OLED_ShowString(0,0,"Welcome to dhter");
	gpio_init();
	OLED_ShowString(0,2,"Init GPIO OK....");

	wifi_init(&wifi_connecting_check_cb);

}
//==============================================================================


/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        case FLASH_SIZE_64M_MAP_1024_1024:
            rf_cal_sec = 2048 - 5;
            break;
        case FLASH_SIZE_128M_MAP_1024_1024:
            rf_cal_sec = 4096 - 5;
            break;
        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}

void ICACHE_FLASH_ATTR user_rf_pre_init(void){}
