#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "mysql_util.h"
#include "log_util.h"

#define PORT 1314
#define BACK_LOG 10

char *g_host_name = "127.0.0.1";
char *g_user_name = "root";
char *g_password = "123456";
char *g_db_name = "fastadmin";
unsigned int g_db_port = 3306;

int parse_arg(char *str, char args[][32])
{
	char *ptr,*retptr;
    int i = 0;
	
	ptr = str;
 	ljf_info("str:%s\n", str);
	
    while ((retptr=strtok(ptr, ",")) != NULL) {
		if(i < 4)
		{
			memcpy(args[i],retptr,strlen(retptr));
		}
		
		ljf_debug("substr[%d]:%s\n", i, retptr);

		i++;
        ptr = NULL;
    }
 	
    return i;
}

int main()
{
    int listenfd,connectfd;
    struct sockaddr_in server;
    struct sockaddr_in client;
    pid_t childpid;
    socklen_t addrlen;
    char buff[4096];  
    ljf_info("Log model init\n");
    if(init_file_point())
    {
        ljf_err("log_init failed,exit...\n");
        exit(1);
    }
	
    ljf_info("Log model init OK\n");
	
    ljf_info("mysql model init\n");
    if (init_mysql(g_host_name,g_user_name,g_password,g_db_name,g_db_port))
    {
        print_mysql_error(NULL);
        ljf_err("mysql init failed,exit...\n");
        exit(1);
    }
    ljf_info("mysql model init OK\n");

    ljf_info("server network model init\n");
    listenfd = socket(AF_INET,SOCK_STREAM,0);
    if(listenfd == -1)
    {
        perror("socker created failed");
        exit(0);
    }
    int option;
    option = SO_REUSEADDR;
    setsockopt(listenfd,SOL_SOCKET,option,&option,sizeof(option));
    bzero(&server,sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listenfd,(struct sockaddr *)&server,sizeof(server)) == -1){
       	perror("Bind error!");
       	exit(1);
    }
    if(listen(listenfd,BACK_LOG) == -1){
       	perror("listend error");
       	exit(1);
    }
    ljf_info("Server network model init OK.\n");
    ljf_info("Listening on port:%d\n", PORT);
    ljf_info("Waiting for clinet's request.....\n");
	
    while(1)
    {
        int n;
    	addrlen = sizeof(client);
    	connectfd = accept(listenfd, (struct sockaddr*)&client, &addrlen);
    	if(connectfd == -1)
	{
        	ljf_err("accept error");
        	exit(1);
    	}
	else
	{
	    ljf_info("########################################################\n");
	    ljf_info("Client from IP:[%s]\n",inet_ntoa(client.sin_addr));
        }
        if((childpid = fork()) == 0)
	{
    	    close(listenfd);
    	    while((n = read(connectfd, buff, 4096)) > 0)
	    {
	        char args[4][32]={0};
		int len = 0;
    		buff[n] = '\0';
    		ljf_info("recv msg from client: %s\n", buff);
#if 0
		len = parse_arg(buff, args);
		/*If args is 4 add to mysql*/
		if(len == 4)
		{
			add_thinfo(args[0], args[1], args[2], args[3], inet_ntoa(client.sin_addr));
 		}
		else
		{
			ljf_info("Invalid args number:%d\n",len);
		}
#endif
 	    }
            exit(0);
    	}
	else if(childpid < 0)
	{
	    ljf_info("fork error: %s\n", strerror(errno));
	}
	else
	{
	    //Father process use wait to avoid zobine process
	    int stat_val;
            wait(&stat_val);
            if (WIFEXITED(stat_val))
            {
                ljf_info("Child process exited with code %d\n", WEXITSTATUS(stat_val));
            }
	    ljf_info("########################################################\n");
	}
    		
    	close(connectfd);
    }
	
    deinit_mysql();
    return 0;
}

