 
/*syslog  等级
 * LOG_EMERG   0   紧急状态
 * LOG_ALERT   1   必须立即修复状态
 * LOG_CRIT    2   严重状态
 * LOG_ERR     3   出错状态
 * LOG_WARNING 4   警告状态
 * LOG_NOTICE  5   正常，但重要的状态
 * LOG_INFO    6   信息性消息
 * LOG_DEBUG   7   调试消息
 */
 
// 主要是因为使用到了  vprintf 、vfprintf、vsyslog等函数，在x86的ubuntu14.04中，不能连续使用，需要拷贝一份 va_list 参数
// vfprintf 连续使用时，需要复制一份， 本文主要是 vprintf、vfprintf的连续使用

#define x86 1
#define DEBUG_LOG_ENABLE


#define PROGRAM "[iotserver] "

#define LOG_EMERG 0
#define LOG_ALERT 1
#define LOG_CRIT 2
#define LOG_ERR 3
#define LOG_WARNING 4
#define LOG_NOTICE 5
#define LOG_INFO 6
#define LOG_DEBUG 7

int init_file_point(void);

void my_syslog(unsigned int lev,const char *msg,...);

#define ljf_err(format,...)  my_syslog(LOG_ERR, PROGRAM  "[%s:%d] " format,__FILE__,__LINE__,##__VA_ARGS__)
#define ljf_info(format,...)  my_syslog(LOG_INFO, PROGRAM  "[%s:%d] " format,__FILE__,__LINE__,##__VA_ARGS__)
#define log_sql(format,...)  my_syslog(LOG_NOTICE, PROGRAM  "[%s:%d] [SQL]" format,__FILE__,__LINE__,##__VA_ARGS__)



#ifdef DEBUG_LOG_ENABLE
#define ljf_debug(format,...)  my_syslog(LOG_DEBUG, PROGRAM  "[%s:%d] " format,__FILE__,__LINE__,##__VA_ARGS__)
#else
#define ljf_debug(format,...)
#endif






