/********************************************************************************

	name:protocol.c

	usage:the protocol MICRO,struct,functions declare

	date:2021-08-09

	author:ljf

********************************************************************************/
#include<stdio.h>
#include <stdlib.h>
#include <string.h>

#include "protocol.h"
#include "mysql_util.h"

int check_dev_access(iot_auth_data_type *auth_data)
{
	int ret = 0;
	
	ret = check_device_match_with_name_secret(auth_data->dev_id, auth_data->secret);
	
	return ret;
}

