#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
 
#include <time.h>
 
#include <syslog.h>
#include <stdarg.h>

#include "log_util.h"
 
unsigned char flag_pf = 1;  //程序是否前台运行的标记，通过命令行传入参数确地是否前台运行，并修改
unsigned char dbg_lev = 7;  //syslog的打印级别，通过命令行传入参数修改 
FILE *fp = NULL;
 
 

 
// 添加时间标记前缀
static void add_time_tag(char *dst,int bufsize,const char *format,...)
{
	time_t now;
	time(&now);
	struct tm *local;
 
	local = localtime(&now);
	strftime(dst,bufsize,"%m-%d %T ",local);
 
        strcat(dst,format);
}


int init_file_point(void)
{
	if(NULL==fp)
		fp = fopen("./test.log","a+");
 
	if(NULL!=fp)
		return 0;
	else
		return -1;
}
 
void my_syslog(unsigned int lev,const char *msg,...)
{
	int len = 0,mal_flag = 0, bufsize = 0;
	char *ptr = NULL;
	char buf[512] = {0};
 
	if(lev<=dbg_lev)    //默认的打印等级
	{
		va_list ap;
		openlog("tag",LOG_PID,LOG_USER);
		va_start(ap,msg);
 
		if( (flag_pf) || (lev==LOG_ERR) )
		{
			len = strlen(msg);
			if(len<(512-40))
			{
				ptr = buf;
				bufsize = 512;
			}
			else
			{
				mal_flag = 1;
				bufsize = len + 40;
				ptr = (char *)malloc(bufsize); 
				bzero(ptr,bufsize);
			}
			add_time_tag(ptr,bufsize,msg);	
		}
 
// 在嵌入式linux中不需要下面条件编译的代码，如果没有下面条件编译部分代码，在ubuntu中运行时会段错误，具体原因未深入研究
#ifdef x86
		va_list map,myap;
		va_copy(map,ap);
		va_copy(myap,ap);
#endif
 
		if(flag_pf)  //如果是前台运行，实现printf功能  
			vprintf(ptr,ap);
 
		if( (NULL != fp)) //如果级别是错误，同时打印到File流文件
#ifdef x86
			vfprintf(fp,ptr,map);
#else
			vfprintf(fp,ptr,ap); // 在嵌入式平台中只需要这行就行
#endif
 
		if(1 == mal_flag)
			free(ptr);
#ifdef x86
		vsyslog(lev,msg,myap);   //syslog有时间前缀，所有不使用增加前缀的ptr指针
#else
		vsyslog(lev,msg,ap);   //syslog有时间前缀，所有不使用增加前缀的ptr指针
#endif
		va_end(ap);
		closelog();
	}
}


