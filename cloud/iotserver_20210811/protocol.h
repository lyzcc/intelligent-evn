/********************************************************************************

	name:protocol.h

	usage:the protocol MICRO,struct,functions declare

	date:2021-08-09

	author:ljf

********************************************************************************/


#define PROTOCOL_NAME "LJF"
#define PROTOCOL_VER "1.0"
#define PROTOCOL_HEADER_MAX 8
#define DEV_ID_MAX 16
#define DEV_SECRET_MAX 16
#define USER_DATA_MAX 1024

typedef enum{
	e_min = 0,
	e_cmd_auth = 1,
	e_cmd_auth_ack = 2,
	e_cmd_thinfo = 3,
	e_cmd_thinfo_ack = 4,
	e_cmd_bye = 5,	/*bye used to disconnect from server*/
	e_cmd_bye_ack = 6, /*When server received bye then response bye ack*/
	e_max
}e_cmd_type;

typedef struct{
	unsigned char dev_id[DEV_ID_MAX];
	unsigned char secret[DEV_SECRET_MAX];
}iot_auth_data_type;

typedef enum{
	e_auth_min = 0,
	e_auth_pass = 1,
	e_auth_no_device,
	e_auth_invalid_secret,
	
	e_auth_max
}e_auth_result;
typedef enum{
	e_success,
	e_failed
}e_operate_result;
/************************
	used to store the auth device info and auth result
*****************************/
typedef struct{
	char dev_id[DEV_ID_MAX];
	e_auth_result auth_result;
	e_operate_result opt_result; //used to store the data handle result such as store the thinfo
	int connectfd;
}msg_process_result_type;

typedef struct{
	unsigned char header[PROTOCOL_HEADER_MAX];
	e_cmd_type cmd;
	unsigned short data_len;
	unsigned char data[USER_DATA_MAX];
}iot_req_data_type;

typedef struct{
	e_cmd_type cmd;
	e_operate_result result;
	char resp_str[128];
}iot_resp_data_type;
int check_dev_access(iot_auth_data_type *auth_data);

