define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'datamanagement/thinfo/index' + location.search,
                    add_url: 'datamanagement/thinfo/add',
                    edit_url: 'datamanagement/thinfo/edit',
                    del_url: 'datamanagement/thinfo/del',
                    multi_url: 'datamanagement/thinfo/multi',
                    import_url: 'datamanagement/thinfo/import',
                    table: 'datamanagement_thinfo',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'devId', title: __('Devid'), operate: 'LIKE'},
                        {field: 'temp', title: __('Temp'), operate: 'LIKE'},
                        {field: 'humi', title: __('Humi'), operate: 'LIKE'},
                        {field: 'timeStamp', title: __('Timestamp'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'reportip', title: __('Reportip'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});