define(['jquery', 'bootstrap', 'backend', 'addtabs', 'table', 'echarts', 'echarts-theme', 'template'], function ($, undefined, Backend, Datatable, Table, Echarts, undefined, Template) {

    var Controller = {
        index: function () {
            // 基于准备好的dom，初始化echarts实例
            var myChart = Echarts.init(document.getElementById('echart'), 'walden');
            getDeviceWeeklyInfo();
            
            $(window).resize(function () {
                myChart.resize();
            });
            /*****************jquery bind event area*************************/
            $("#deviceSelect").change(function(data){
                getDeviceWeeklyInfo();
            });

            $(document).on("click", ".btn-refresh", function () {
                setTimeout(function () {
                    myChart.resize();
                }, 0);
            });
            /*********************private function area*****************************/
            function getDeviceWeeklyInfo(){
                var devicename = $("#deviceSelect").val();
                console.info("select "+devicename);
                $.ajax({
                        method: 'get',
                        url:"dashboard/get_weekly_ehcart_info?&devicename="+devicename,
                        success: function (data) {
                            console.info(data);
                            updateWeeklyEchart(data);
                        }
                });
            }
            function updateWeeklyEchart(data){
                var option = {
                    title: {
                        text: '',
                        subtext: ''
                    },
                    color: [
                        "#18d1b1",
                        "#3fb1e3",
                        "#626c91",
                        "#a0a7e6",
                        "#c4ebad",
                        "#96dee8"
                    ],
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: [__('Daily high'),__('Daily low')]
                    },
                    toolbox: {
                        show: false,
                        feature: {
                            magicType: {show: true, type: ['stack', 'tiled']},
                            saveAsImage: {show: true}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: data.column
                    },
                    yAxis: {},
                    grid: [{
                        left: 'left',
                        top: 'top',
                        right: '10',
                        bottom: 30
                    }],
                    series: [{
                        name: __('Daily high'),
                        type: 'line',
                        smooth: true,
                        areaStyle: {
                            normal: {}
                        },
                        lineStyle: {
                            normal: {
                                width: 1.5
                            }
                        },
                        data: data.weeklyhigh
                    },{
                        name: __('Daily low'),
                        type: 'line',
                        smooth: true,
                        areaStyle: {
                            normal: {}
                        },
                        lineStyle: {
                            normal: {
                                width: 1.5
                            }
                        },
                        data: data.weeklylow
                    }]
                };
                myChart.setOption(option);
            }
        }
    };

    return Controller;
});
