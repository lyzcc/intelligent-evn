<?php
#require __DIR__ . '/vendor/autoload.php';
use Stomp\Client;
use Stomp\Network\Observer\Exception\HeartbeatException;
use Stomp\Network\Observer\ServerAliveObserver;
use Stomp\StatefulStomp;
function start_consume() {
    //参数说明，请参见AMQP客户端接入说明文档。
    $accessKey = "LTAI4G3HrByuULP9sKFDj4cs";
    $accessSecret = "GNKYNJIkU3zP4uhKA7O5im3sVOzOy1";
    $consumerGroupId = "j9TuNXnU9F41CU2bJK3U000100";
    $clientId = "461af9ec-32d0-4c6f-b05b-20fbf693e783";
    //iotInstanceId：购买的实例请填写实例ID，公共实例请填空字符串""。
    $iotInstanceId = "";
    $timeStamp = round(microtime(true) * 1000);
    //签名方法：支持hmacmd5，hmacsha1和hmacsha256。
    $signMethod = "hmacsha1";
    //userName组装方法，请参见AMQP客户端接入说明文档。
    //若使用二进制传输，则userName需要添加encode=base64参数，服务端会将消息体base64编码后再推送。具体添加方法请参见下一章节“二进制消息体说明”。
    $userName = $clientId . "|authMode=aksign"
                . ",signMethod=" . $signMethod
                . ",timestamp=" . $timeStamp
                . ",authId=" . $accessKey
                . ",iotInstanceId=" . $iotInstanceId
                . ",consumerGroupId=" . $consumerGroupId
                . "|";
    $signContent = "authId=" . $accessKey . "&timestamp=" . $timeStamp;
    //计算签名，password组装方法，请参见AMQP客户端接入说明文档。
    $password = base64_encode(hash_hmac("sha1", $signContent, $accessSecret, $raw_output = TRUE));
    //接入域名，请参见AMQP客户端接入说明文档。
    $client = new Client('ssl://${YourHost}:61614');
    $sslContext = ['ssl' => ['verify_peer' => true, 'verify_peer_name' => false], ];
    $client->getConnection()->setContext($sslContext);

    //服务端心跳监听。
    $observer = new ServerAliveObserver();
    $client->getConnection()->getObservers()->addObserver($observer);
    //心跳设置，需要云端每30s发送一次心跳包。
    $client->setHeartbeat(0, 30000);
    $client->setLogin($userName, $password);
    try {
        $client->connect();
    }
    catch(StompException $e) {
        echo "failed to connect to server, msg:" . $e->getMessage() , PHP_EOL;
    }
    //无异常时继续执行。
    $stomp = new StatefulStomp($client);
    $stomp->subscribe('/topic/#');
    return $stomp;
}

$stomp = start_consume();

while (true) {
    if ($stomp == null || !$stomp->getClient()->isConnected()) {
        echo "connection not exists, will reconnect after 30s.", PHP_EOL;
        sleep(10);
        $stomp = start_consume();
    }

    try {
        //处理消息业务逻辑。
        echo $stomp->read();
    }
    catch(HeartbeatException $e) {
        echo 'The server failed to send us heartbeats within the defined interval.', PHP_EOL;
        $stomp->getClient()->disconnect();
    } catch(Exception $e) {
        echo 'process message occurs error '. $e->getMessage() , PHP_EOL;
    }
}   