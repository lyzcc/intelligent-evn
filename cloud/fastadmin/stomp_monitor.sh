#/bin/sh
LOG_FILE=/var/www/intelligent-evn/cloud/fastadmin/runtime/log/stomp_monitor.log
proc_name="php think stomp"
if [ $# != 1 ]; then
	echo "Use default php think stomp " >> ${LOG_FILE}
else
	proc_name=$1
fi
echo ${proc_name}
DATETIME=`date +"%y-%m-%d %H:%M:%S"`

ps -fe | grep "${proc_name}" | grep -v grep
if [ $? -ne 0 ]; then
	echo "${DATETIME} : ${proc_name} not running, Start process....." >> ${LOG_FILE}
	cd /var/www/intelligent-evn/cloud/fastadmin/
	nohup php think stomp & >> ${LOG_FILE}
else
	echo "${DATETIME} : ${proc_name} is runing......" >> ${LOG_FILE}
fi
