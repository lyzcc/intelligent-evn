<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\admin\model\datamanagement\Thinfo as Thbk;
use app\admin\model\devmgmt\Device;
use think\Db;
use think\Log;
use fast\Date;

class Thinfo extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    private function print($msg)
    {
        LOG::write($msg,'info');
    }
    private function get_device_list()
    {
        $devlist = [];
        $list =  Device::select();
        foreach ($list as $k => $v) {
            $devlist[] = $v['devicename'];
        }
        return $devlist;
    }
    public function index()
    {
        $devlist = $this->get_device_list();


        $lastData = Thbk::where('devId' , '=', $devlist[0])
            ->field('devId, temp, humi, timeStamp')
            ->limit(1)
            ->order('timeStamp', 'desc')
            ->select();
        
        foreach ($lastData as $k => $v) {
            $temp = $v['temp'];
            $humi = $v['humi'];
            $timeStamp = $v['timeStamp'];
        }
        $this->print("temp:".$temp.",humi:".$humi);
        try {
            \think\Db::execute("SET @@sql_mode='';");
        } catch (\Exception $e) {

        }
        $list = Thbk::where('devId' , '=', $devlist[0])
            ->field('devId, temp, humi, timeStamp')
            ->limit(144)
            ->order('timeStamp', 'desc')
            ->select();
       
        foreach ($list as $k => $v) {
            $report_time = date('H:m:s',strtotime($v['timeStamp']));
            $templist[$report_time] = $v['temp'];
            $humilist[$report_time] = $v['humi'];
        }
        //$this->print($templist);
        $templist = array_reverse($templist);
        $humilist = array_reverse($humilist);
        //$this->print(array_keys($templist));
        $weeklytemp_hi = [];
        $weeklytemp_low = [];
        $starttime = date("Y-m-d H:m:s",Date::unixtime('day', -7));
        $endtime = date("Y-m-d H:m:s",Date::unixtime('day', 0, 'end'));
        $this->print("start:".$starttime.",endtime:".$endtime);

        $weeklylist = Thbk::where('devId' , '=', $devlist[0])
                        ->where('timeStamp', 'between time', [$starttime, $endtime])
                        ->field('MAX(temp) as max_temp, MIN(temp) as min_temp,  DATE_FORMAT(timeStamp, "%y-%m-%d") AS report_date')
                        ->group('report_date')
                        ->limit(7)
                        ->select();
        
        foreach ($weeklylist as $k => $v) {
            $weeklytemp_hi[$v['report_date']] = $v['max_temp'];
            $weeklytemp_low[$v['report_date']] = $v['min_temp'];
        }
        $this->print($weeklytemp_hi);
        
        $this->view->assign('currentDev', $devlist[0]);
        $this->view->assign('devList', $devlist);
        
        $this->view->assign('temp', $temp);
        $this->view->assign('humi', $humi);
        $this->view->assign('timeStamp', $timeStamp);
        $this->view->assign('title', __('Thinfo'));
        $this->view->assign('column', json_encode(array_keys($templist)));
        $this->view->assign('temperaturedata', json_encode(array_values($templist)));
        $this->view->assign('humiditydata', json_encode(array_values($humilist)));
        $this->view->assign('weeklytemp_col', json_encode(array_keys($weeklytemp_hi)));
        $this->view->assign('weeklytemp_hi', json_encode(array_values($weeklytemp_hi)));
        $this->view->assign('weeklytemp_low', json_encode(array_values($weeklytemp_low)));
        

        return $this->view->fetch();
    }

    public function get_data_by_devname() {
        $devid = $this->request->get("devicename", '');
        $this->print("devicename=".$devid);
        $temp = NULL;
        $humi = NULL;
        $timeStamp = NULL;
        $weeklytemp_hi = [];
        $weeklytemp_low = [];
        $templist = [];
        $humilist = [];

        $lastData = Thbk::where('devId' , '=', $devid)
                        ->field('devId, temp, humi, timeStamp')
                        ->order('timeStamp', 'desc')
                        ->limit(1)
                        ->select();
    
        foreach ($lastData as $k => $v) {
            $temp = $v['temp'];
            $humi = $v['humi'];
            $timeStamp = $v['timeStamp'];
        }
        $this->print("temp:".$temp.",humi:".$humi);

        $starttime = date("Y-m-d H:m:s",Date::unixtime('day', -7));
        $endtime = date("Y-m-d H:m:s",Date::unixtime('day', 0, 'end'));
        $this->print("start:".$starttime.",endtime:".$endtime);

        $weeklylist = Thbk::where('devId' , '=', $devid)
                        ->where('timeStamp', 'between time', [$starttime, $endtime])
                        ->field('MAX(temp) as max_temp, MIN(temp) as min_temp,  DATE_FORMAT(timeStamp, "%y-%m-%d") AS report_date')
                        ->group('report_date')
                        ->limit(7)
                        ->select();
        
        foreach ($weeklylist as $k => $v) {
            $weeklytemp_hi[$v['report_date']] = $v['max_temp'];
            $weeklytemp_low[$v['report_date']] = $v['min_temp'];
        }

        $list = Thbk::where('devId' , '=', $devid)
            ->field('devId, temp, humi, timeStamp')
            ->limit(144)
            ->order('timeStamp', 'desc')
            ->select();
       
        foreach ($list as $k => $v) {
            $report_time = date('H:m:s',strtotime($v['timeStamp']));
            $templist[$report_time] = $v['temp'];
            $humilist[$report_time] = $v['humi'];
        }
        $templist = array_reverse($templist);
        $humilist = array_reverse($humilist);

        $result = [
                    'devname' => $devid,
                    'temp' => $temp,
                    'humi' => $humi,
                    'timeStamp' => $timeStamp,
                    'column' => array_keys($templist),
                    'temperaturedata' => array_values($templist),
                    'humiditydata' => array_values($humilist),
                    'weeklytemp_col' => array_keys($weeklytemp_hi),
                    'weeklytemp_hi' => array_values($weeklytemp_hi),
                    'weeklytemp_low' => array_values($weeklytemp_low),
                  ];
        return $result;
    }

}
