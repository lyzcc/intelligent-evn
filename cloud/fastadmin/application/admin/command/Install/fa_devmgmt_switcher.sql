﻿# Host: localhost  (Version: 5.7.26)
# Date: 2022-03-18 14:51:38
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "fa_devmgmt_switcher"
#

DROP TABLE IF EXISTS `fa_devmgmt_switcher`;
CREATE TABLE `fa_devmgmt_switcher` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `devicename` varchar(128) NOT NULL DEFAULT '' COMMENT '设备名称',
  `on` enum('0','1') DEFAULT '0' COMMENT '开关:0=关,1=开',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(11) unsigned DEFAULT NULL COMMENT '修改时间',
  `admin_id` int(11) DEFAULT NULL COMMENT '管理员ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='开关';
