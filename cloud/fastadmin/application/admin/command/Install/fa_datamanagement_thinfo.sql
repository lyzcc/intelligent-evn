﻿
DROP TABLE IF EXISTS `fa_datamanagement_thinfo`;
CREATE TABLE `fa_datamanagement_thinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `devId` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '设备ID',
  `temp` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '温度(℃)',
  `humi` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '湿度(%RH)',
  `timeStamp` datetime NOT NULL COMMENT '更新时间',
  `reportip` varchar(32) DEFAULT NULL COMMENT '远端IP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf32 COMMENT='温湿度';
