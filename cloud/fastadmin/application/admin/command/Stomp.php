<?php
namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Request;
use Stomp\Client;

class Stomp extends Command {
    protected function configure() {
        // 进程名，下文命令行中会体现
        $this->setName('stompsub')->setDescription('订阅打Stomp');
    }

    protected function execute(Input $input, Output $output) {
        //$request = new Request();
        // 被执行代码所在模块
        //$request->module("api");
        // 控制器和方法
        $output->writeln(controller('api/Amqpgetmsg')->index());
    }
};
