<?php

return [
    'Id'             => 'ID',
    'Devicename'     => '设备名称',
    'Devicesecrit'   => '密钥',
    'State'          => '在线状态',
    'State 0'        => '在线',
    'State 1'        => '离线',
    'Signalstrength' => '信号强度',
    'Reportinterval' => '上报周期（ms）',
    'Heartbeat'      => '心跳周期（ms）',
    'Createtime'     => '创建时间',
    'Updatetime'     => '修改时间',
    'Admin_id'       => '管理员ID'
];
