<?php

return [
    'Id'         => 'ID',
    'Devicename' => '设备名称',
    'On'         => '开关',
    'On 0'       => '关',
    'On 1'       => '开',
    'Createtime' => '创建时间',
    'Updatetime' => '修改时间',
    'Admin_id'   => '管理员ID'
];
