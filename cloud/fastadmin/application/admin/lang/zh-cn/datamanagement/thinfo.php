<?php

return [
    'Id'        => 'ID',
    'Devid'     => '设备ID',
    'Temp'      => '温度(℃)',
    'Humi'      => '湿度(%RH)',
    'Timestamp' => '更新时间',
    'Reportip'  => '远端IP'
];
