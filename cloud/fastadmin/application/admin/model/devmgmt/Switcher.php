<?php

namespace app\admin\model\devmgmt;

use think\Model;


class Switcher extends Model
{

    

    

    // 表名
    protected $name = 'devmgmt_switcher';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'on_text'
    ];
    

    
    public function getOnList()
    {
        return ['0' => __('On 0'), '1' => __('On 1')];
    }


    public function getOnTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['on']) ? $data['on'] : '');
        $list = $this->getOnList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
