<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\admin\model\datamanagement\Thinfo as th;
use think\Db;
use think\Log;

/**
 * 示例接口
 */
class Thinfo extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];
    private function print($msg)
    {
        LOG::write($msg,'info');
    }
    /**
     * 测试方法
     *
     * @ApiTitle    (测试名称)
     * @ApiSummary  (测试描述信息)
     * @ApiMethod   (POST)
     * @ApiRoute    (/api/demo/test/id/{id}/name/{name})
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="integer", required=true, description="会员ID")
     * @ApiParams   (name="name", type="string", required=true, description="用户名")
     * @ApiParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据")
     * @ApiReturnParams   (name="code", type="integer", required=true, sample="0")
     * @ApiReturnParams   (name="msg", type="string", required=true, sample="返回成功")
     * @ApiReturnParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据返回")
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功'
        })
     */
    public function test()
    {
        $this->success('返回成功', $this->request->param());
    }

    public function getlastthinfo()
    {
        $ret = 0;
        $lastData = Db("datamanagement_thinfo")->field('devId, temp, humi, timeStamp')
            ->limit(1)
            ->order('timeStamp', 'desc')
            ->select();

        $result=NULL;

        foreach ($lastData as $k => $v) {
            $result = array(
                "temp" => $v['temp'],
                "humi" => $v['humi'],
                "timeStamp" => $v["timeStamp"],
            );
        }

        $this->success("Success", ['result' => $result]);
    }

    public function getthinfolist()
    {
        try {
            \think\Db::execute("SET @@sql_mode='';");
        } catch (\Exception $e) {

        }

        $list = Db("datamanagement_thinfo")
            ->field('devId, temp, humi, timeStamp')
            ->limit(96)
            ->order('timeStamp', 'desc')
            ->select();
       
        foreach ($list as $k => $v) {
            $templist[$v['timeStamp']] = $v['temp'];
            $humilist[$v['timeStamp']] = $v['humi'];
        }
        $this->print("####################");
        $this->print($templist);
        $templist = array_reverse($templist);
        $humilist = array_reverse($humilist);

        $this->success("Success", ['column' => array_keys($templist),
                                'temperaturedata' => array_values($templist),
                                'humiditydata' => array_values($humilist)
                                ]);
    }

}
