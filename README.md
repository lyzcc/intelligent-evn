# IntelligentEvn

#### 介绍
基于fastadmin的环境数据采集监测，采用esp8266模块+dht11做数据采集，fastadmin进行数据展示，iotserver作为接受采集数据并转存格式的中间件

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/172604_994c9a24_4817511.png "微信截图_20210726172545.png")

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  fastadmin安装配置
2.  esp8266编译烧录
3.  iotserver启动

#### 参与贡献


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
